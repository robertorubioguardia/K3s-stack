## Despliegue de cluster de K3s (https://k3s.io) con un master y dos workers desde una máquina local que ya tenga nfs-common, Docker y docker-compose instalado
#
# Requiere de tres máquinas instaladas con Ubuntu Server 18.04 ó 20.x y acceso SSH 
#
# IMPORTANTE: Este script asume que NO cuentas con un archivo ~/.kube/config. Si es asi, debes renombrarlo antes.

# Crear la carpeta de configuración de kubectl
mkdir ~/.kube

echo "Iniciamos la instalación de K3s"

# Ingresamos las IP de los servidores a usar

echo "Ingresa un nombre para el cluster (Sin espacios. Puedes utilizar guiones)"
read CONTEXT

echo "Ingresa la IP del master"
read MASTER_IP

echo "Ingresa la IP del worker 1"
read NODE1_IP

echo "Ingresa la IP del worker 2"
read NODE2_IP

# Instalar el master y los dos nodos

echo "Instalando el master"
k3sup install \
  --ip $MASTER_IP \
  --cluster \
  --user root \
  --ssh-key $HOME/.ssh/id_rsa_opsrobot \
  --k3s-channel stable \
  --local-path ~/.kube/config \
  --merge --context $CONTEXT \
  --k3s-extra-args '--disable traefik --write-kubeconfig-mode 644'

echo "Instalando el worker 1"
k3sup join \
  --server-ip $MASTER_IP \
  --ip $NODE1_IP \
  --user root \
  --ssh-key $HOME/.ssh/id_rsa_opsrobot \
  --k3s-channel stable

echo "Instalando el worker 2"
k3sup join \
  --server-ip $MASTER_IP \
  --ip $NODE2_IP \
  --user root \
  --ssh-key $HOME/.ssh/id_rsa_opsrobot \
  --k3s-channel stable

echo "Instalación de K3s terminada"

# Instalar NGINX como Ingress

echo "Instalando NGINX como ingress"
arkade install ingress-nginx --namespace default
arkade install cert-manager

# Instalar Longhorn como controlador de almacenamiento

echo "Instanado Longhorn como controlador de almacenamiento"
helm repo add longhorn https://charts.longhorn.io
helm repo update
kubectl create namespace longhorn-system
helm install longhorn longhorn/longhorn --namespace longhorn-system

echo "El cluster ha sido desplegado completamente"

## Estos pasos son opcionales y sugeridos para confirmar que nuestro cluster ha quedado bien instalado:
#
# kubectl get nodes # verificamos que todos los nodos existan y estén en status READY
#
# Verificar que Longhorn esté bien instalado:
#
# kubectl -n longhorn-system get pod
#
## Acceder a la UI de Longhorn para confirmar los detalles:
#
# kubectl -n longhorn-system get svc
# kubectl port-forward -n longhorn-system svc/longhorn-frontend 8002:80
#
# Accede desde tu browser a http://localhost:8002