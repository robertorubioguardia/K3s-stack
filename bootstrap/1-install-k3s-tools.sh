## Instalamos las herramientas requeridas para el despliegue (Opcional. Sólo si no tienes las herramientas ya instaladas.

echo "Instalando herramientas requeridas"

# Arkade
curl -SLfs https://dl.get-arkade.dev | sudo sh
# Add tools bin directory to PATH
echo "export PATH=\$HOME/.arkade/bin:\$PATH" >> ~/.bashrc
# Copy bash completion script
arkade completion bash > ~/arkade_bash_completion.sh
echo "source ~/arkade_bash_completion.sh" >> ~/.bashrc
# starting bash completion
set -i source ~/.bashrc

# Kubectl
arkade get kubectl
# Install Kubectl
sudo mv $HOME/.arkade/bin/kubectl /usr/local/bin/
# Kubectl bash completion
echo 'source <(kubectl completion bash)' >>~/.bashrc
# starting bash completion
set -i source ~/.bashrc

# Kustomize
arkade get kustomize
#Install kustomize
sudo mv $HOME/.arkade/bin/kustomize /usr/local/bin/

# Helm
arkade get helm
# Install helm
sudo mv $HOME/.arkade/bin/helm /usr/local/bin/

# K3sup
arkade get k3sup
# Install k3sup
sudo mv $HOME/.arkade/bin/k3sup /usr/local/bin/

# Kompose for converting docker-compose

# Para AMD64: 
# curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose

# Para MacOS (Intel o M1 usando Rosetta)
curl -L https://github.com/kubernetes/kompose/releases/download/v1.24.0/kompose-darwin-amd64 -o kompose

chmod +x kompose
sudo mv ./kompose /usr/local/bin/kompose
echo 'source <(kompose completion bash)' >>~/.bashrc
set -i source ~/.bashrc