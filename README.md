# Stack de OpsRobot sobre K3s

Este repositorio contiene lo necesario para desplegar un cluster de Kubernetes, basado en la distro [K3s de Rancher Labs](https://k3s.io). Estaremos actualizando este documento constantemente con nuevos componentes y mejoras.

El cluster está formado por tres nodos:

- Master (control-plane)
- Worker 1
- Worker 2

Además de K3s, también se despliegan y configuran automáticamente nginx como "ingress", Longhorn como storage-class replicado y de forma opcional, permission-manager para gestionar los usuarios y roles de tu cluster y prometheus como sistema de monitoreo y gestión de alertas.

Incluimos una carpeta `configs` con algunas configuraciones opcionales adicionales, por ejemplo, utilizar un load balanacer nginx externo en lugar de nginx como ingress.
 
Por contar con un solo nodo `Master`, no se le puede considerar un cluster HA. Es posible modificar el script de bootstrap del cluster para incluir dos masters, pero ya queda a criterio de cada uno.

## Instalar las herramientas de gestión y despliegue:

Primero debemos instalar las herramientas que utilizaremos para administrar y desplegar nuestro cluster de K3s

Utilizaremos un script (así que no corras a hacerlo manualmente), pero te dejo la lista de las herramientas que se instalan:

1. [Arkade](https://github.com/alexellis/arkade) - Marketplace para Kubernetes
2. [Kubectl](https://kubernetes.io/docs/reference/kubectl/) - Herramienta de línea de comandos (CLI) de Kubernetes
3. [Kustomize](https://kustomize.io/) - Config Manager para Kubernetes
4. [Helm](https://helm.sh/) - Gestor de paquetes para Kubernetes
5. [K3sup (Pronunciado Ketchup)](https://github.com/alexellis/k3sup) - Herramienta de despliegue de K3s en ambientes locales o remotos

### Instalación de las herramientas:

1. Clona este repo a tu equipo local o desde donde vayas a administrar/desplegar tu cluster

```bash
git clone https://gitlab.com/opsrobot/k3s-stack
```

2. Ingresa a la carpeta `bootstrap` y modifica el archivo `1-install-k3s-tools.sh`, comentando/descomentando las líneas 42 y 45, según tu sistema operativo

```bash
cd bootstrap
```
```bash
nano 1-install-k3s-tools
```
Por ejemplo, para Mac debería ser así:

```
# Para Linux/WSL en Windows (x86_64/AMD64): 
# curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose

# Para MacOS (Intel o M1 usando Rosetta)
curl -L https://github.com/kubernetes/kompose/releases/download/v1.24.0/kompose-darwin-amd64 -o kompose
```

3. Ejecuta:

```bash
sh 1-install-k3s-tools.sh
```

## Despliegue del cluster de K3s

### Preparación de los nodos:

1. Prepara tres máquinas (físicas o virtuales, en cloud, premisas o como quieras) con Ubuntu 18.04 o 20.04. Te recomendamos al menos 2GB de ram y 2 vcpu para una prueba básica. 8GB de ram y 3 vcpu para un uso productivo. El espacio de almacenamiento mínimo de al menos 25GB por nodo es recomendado

2. Asegúrate de que se encuentren en la misma red o que tengan visibilidad entre ellas

3. Asegúrate que tienes acceso SSH a las tres, via llaves de SSH (o cualquier otra forma que no sea una contraseña), desde el equipo donde instalaste las herramientas.

4. Anota las direcciones IP de las tres máquinas porque el script de despliegue del cluster te las pedirá

### Despliegue del cluster:

1. Desde la carpeta `bootstrap` ejecuta:

```bash
sh deploy-k3s-cluster.sh
```

2. Ingresa los datos solicitados por el script de forma interactiva

3. Al terminar el despliegue, revisa en la carpeta `~/.kube` y confirma que contiene el archivo `config`

4. Puedes probar tu cluster ejecutando:
```bash
kubectl get nodes
```

Si todo resulta bien, debes ver algo como esto:

```
NAME                STATUS   ROLES                       AGE    VERSION
k3s-01-sr-master    Ready    control-plane,etcd,master   4d9h   v1.22.7+k3s1
k3s-01-sr-worker1   Ready    <none>                      4d9h   v1.22.7+k3s1
k3s-01-sr-worker2   Ready    <none>                      4d9h   v1.22.7+k3s1
```


## Desplegando los componentes opcionales

### Permission Manager

- En proceso

### Prometheus

- En proceso



